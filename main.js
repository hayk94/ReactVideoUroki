import React from 'react'
import ReactDOM from 'react-dom'

import App from './components/App.js'

ReactDOM.render(<App
  name='Hayk'
  lastName='Safaryan'
/>, document.querySelector('#app'))

// DONE: JSX +
// TODO: COMPONENT
// // DONE: Function
// // TODO: Class
// DONE: PROPS
// TODO: STATE
