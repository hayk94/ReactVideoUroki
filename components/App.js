import React from 'react'

const App = ({ name, lastName }) => <div>Hello {`${name} ${lastName}`}</div>

export default App
